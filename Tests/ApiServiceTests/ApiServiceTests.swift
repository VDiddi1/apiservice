import XCTest
@testable import ApiService

final class ApiServiceTests: XCTestCase {
    
    // MARK: - PROPERTIES
    var urlSession: URLSession {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        return URLSession(configuration: configuration)
    }
    var apiService: ApiService!
    
    // MARK: - SET-UP CODE
    override func setUp() async throws {
        
        apiService = ApiService(urlSession: urlSession,
                                dependencies: ApiService.Dependencies.init(
                                    generatePopToken: { urlRequest in
                                        print("Generating POP Token")
                                        return "PopTokenValue"
                                    }, logRequest: { request in
                                        print("Request: \(request)")
                                    }, logResponse: { response, data, error, startDate in
                                        print("Response: \(response)")
                                        if let error = error {
                                            print("Error: \(error)")
                                        }
                                        if let data = data {
                                            print("Data: \(data)")
                                        }
                                        print("Request Time: \(startDate)")
                                    }),
                                config: [ .accessToken: "DummyAccessToken",
                                          .storeId: "test",
                                          .ntId: "pivottest2",
                                          .interactionId: "1234"])
    }
    
    // MARK: - TEST SUCCESS
    func testSuccess() async throws {
        
        // Assuming a request is made to this URL in the application
        let url = URL(string: "http://echo.jsontest.com/key/value/one/two")
        guard let url = url else { return }
        
        try MockURLProtocol.response(for: url, response: DummyReponse(one: "two", key: "value"))
        
        do {
            let responseData: Data  = try await apiService.deleteRequest(to: url )
            
            guard let decodedResponse = try? apiService.decoder.decode(DummyReponse.self, from: responseData) else {
                throw APIProtocolErrors.undecodableResponse
            }
            XCTAssertTrue(decodedResponse.one == "two")
        } catch {
            XCTAssertTrue(false, error.localizedDescription)
        }
    }
}

// MARK: - DUMMY REPONSE MODEL
struct DummyReponse: Codable {
    let one: String
    let key: String
}
