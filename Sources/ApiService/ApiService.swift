//
//  TMOAPI.swift
//  TMOAPI Module
//
//

import Foundation
import os


/// An object that handles API requests for RetailOne
///
/// Example usage:
/// ```swift
///    // Initialize ApiService. Custom URLSession, dependecy closures and config values could also be passed.
///    let apiService = ApiService()
///
///    guard let url = URL(string: "http://echo.jsontest.com/key/value/one/two") else { return }
///    do {
///      let response: ResponseObject  = try await apiService.getRequest(to: url )
///      print(responseObject)
///    } catch {
///      print(error.localizedDescription)
///    }
/// ```

// MARK: - API SERVICE PROTOCOLS
public protocol ApiServiceProtocol {
    func getRequest(to url: URL) async throws -> Data
    func postRequest<T: Encodable>(to url: URL, encodable: T) async throws -> Data
    func putRequest<T: Encodable>(to url: URL, encodable: T) async throws -> Data
    func patchRequest<T: Encodable>(to url: URL, encodable: T) async throws -> Data
    func deleteRequest(to url: URL) async throws -> Data
}

// MARK: - API SERVICE
public class ApiService: ApiServiceProtocol {
    
    // MARK: - PROPERTIES
    var decoder: JSONDecoder = JSONDecoder()
    var encoder: JSONEncoder = JSONEncoder()
    
    enum HTTPMethod: String {
        case POST
        case GET
        case PUT
        case PATCH
        case DELETE
    }
    
    public enum ConfigKey: String {
        case ntId
        case storeId
        case accessToken
        case interactionId
    }
    
    public struct Dependencies {
        var generatePopToken: ((URLRequest) -> String)?
        var logRequest: ((URLRequest) -> Void)?
        var logResponse: ((URLResponse, Data?, Error?, Date) -> Void)?
    }
    
    let dependencies: Dependencies?
    
    let config: [ConfigKey: String]?
    let urlSession: URLSession
    
    
    /// Initialize ApiService. All params are optional, however most clients will pass in a cusomt URLSession, Dependecy closures and config values.
    /// - Parameters:
    ///   - urlSession: Custom URLSession.
    ///   - dependencies: Dependecy closures to handle logging, POP token generation etc.
    ///   - config: Configuration values. storeId, accesstoken etc.
    public init(urlSession: URLSession = URLSession.shared, dependencies: Dependencies?=nil, config: [ConfigKey: String]?=nil) {
        self.urlSession = urlSession
        self.dependencies = dependencies
        self.config = config
    }
    
    /// HTTP GET request.
    /// - Parameter url: The URL of the receiver
    /// - Returns: Decoded response object
    public func getRequest(to url: URL) async throws -> Data {
        return try await sendRequest(to: url, httpMethod: .GET)
    }
    
    /// HTTP POST request
    /// - Parameters:
    ///   - url: The URL of the receiver
    ///   - encodable: The encodable struct that contains properties that form the request body
    /// - Returns: Decoded response object
    public func postRequest<T: Encodable>(to url: URL, encodable: T) async throws -> Data {
        return try await sendRequest(to: url, httpMethod: .POST, encodable: encodable)
    }
    
    /// HTTP PUT request
    /// - Parameters:
    ///   - url: The URL of the receiver
    ///   - encodable: The encodable struct that contains properties that form the request body
    /// - Returns: Decoded response object
    public func putRequest<T: Encodable>(to url: URL, encodable: T) async throws -> Data {
        return try await sendRequest(to: url, httpMethod: .PUT, encodable: encodable)
    }
    
    /// HTTP PATCH request
    /// - Parameters:
    ///   - url: The URL of the receiver
    ///   - encodable: The encodable struct that contains properties that form the request body
    /// - Returns: Decoded response object
    public func patchRequest<T: Encodable>(to url: URL, encodable: T) async throws -> Data {
        return try await sendRequest(to: url, httpMethod: .PATCH, encodable: encodable)
    }
    
    /// HTTP POST request
    /// - Parameters:
    ///   - url: The URL of the receiver
    /// - Returns: Decoded response object
    public func deleteRequest(to url: URL) async throws -> Data {
        return try await sendRequest(to: url, httpMethod: .DELETE)
    }
    
    /// Send an HTTP request with encodable body and decodable response
    /// - Parameters:
    ///   - url: The URL of the receiver
    ///   - httpMethod: HTTP Method to use
    ///   - encodable: The encodable struct that contains properties that form the request body
    /// - Returns: Decoded response object
    func sendRequest<T: Encodable>(to url: URL, httpMethod: HTTPMethod, encodable: T) async throws -> Data {
        guard let requestData = try? encoder.encode(encodable) else {
            throw APIProtocolErrors.unencodableInput
        }
        
        return try await sendRequest(to: url, httpMethod: httpMethod, body: requestData)
    }
    
    /// Send an HTTP request with a decodable response
    /// - Parameters:
    ///   - url: The URL of the receiver
    ///   - httpMethod: HTTP Method to use
    /// - Returns: Decoded response object
    func sendRequest(to url: URL, httpMethod: HTTPMethod) async throws -> Data {
        
        return try await sendRequest(to: url, httpMethod: httpMethod)
    }
    
    /// Send an HTTP request
    /// - Parameters:
    ///   - url: The URL of the receiver
    ///   - httpMethod: HTTP Method to use
    ///   - body: Body data object
    /// - Returns: Data response object
    func sendRequest(to url: URL, httpMethod: HTTPMethod, body: Data? = nil) async throws -> Data {
        let startDate = Date()
        var request = URLRequest(url: url)
        
        request.httpMethod = httpMethod.rawValue
        if let body = body {
            request.httpBody = body
            request.addValue("application/json", forHTTPHeaderField: Headers.Key.contentType)
        }
        
        request.allHTTPHeaderFields = generateHeaders()
        
        if let popToken = dependencies?.generatePopToken?(request) {
            request.addValue(popToken, forHTTPHeaderField: Headers.Key.xauthorization)
        }
        
        dependencies?.logRequest?(request)
        let (data, response) = try await urlSession.data(for: request)
        guard let httpResponse = response as? HTTPURLResponse else {
            let error = APIProtocolErrors.unexpectedResponse
            dependencies?.logResponse?(response, nil, error, startDate)
            throw error
        }
        do {
            try httpResponse.throwForErrors()
        } catch let error {
            dependencies?.logResponse?(httpResponse, data, error, startDate)
            throw error
        }
        dependencies?.logResponse?(httpResponse, data, nil, startDate)
        return data
    }
}


// MARK: - EXTENSION OF API SERVICE
extension ApiService {
    
    // MARK: - GENERATE HEADERS
    func generateHeaders() -> [String: String] {
        let accessToken = config?[.accessToken] ?? ""
        let headers: [String: String] = [
            Headers.Key.authorization: "Bearer \(accessToken)",
            Headers.Key.accept: "application/json",
            Headers.Key.xauthOriginator: accessToken,
            Headers.Key.channelId: "WEB",
            Headers.Key.applicationId: "TMO",
            Headers.Key.lineOfBusiness: "MAGENTA",
            Headers.Key.workflowId: "ACTIVATION",
            Headers.Key.subWorkflowId: "POSTPAID",
            Headers.Key.activityId: getActivityId(),
            Headers.Key.sessionId: "test",
            Headers.Key.baggageId: "test",
            Headers.Key.searchResultTemplate: "TRIM",
            Headers.Key.interactionId: config?[.interactionId] ?? "",
            Headers.Key.serviceTransactionId: serviceTransactionId(),
            Headers.Key.storeId: config?[.storeId] ?? "",
            Headers.Key.partnerId: "fdsf",
            Headers.Key.originApplicationId: "fsfd",
            Headers.Key.follow: "true"
        ]
        return headers
    }
    
    // MARK: - GET ACTIVITY ID
    func getActivityId() -> String {
        return "\(config?[.ntId] ?? "")_\(Date.milliSecondsSince1970)"
    }
    
    // MARK: - GET SERVICE TRANSACTION ID
    func serviceTransactionId() -> String {
        return UUID().uuidString
    }
}

// MARK: - HEADERS
struct Headers {
    struct Key {
        static let accept = "accept"
        static let contentType = "content-type"
        static let date = "date"
        static let interactionId = "interaction-id"
        static let dealerCode = "dealer-code"
        static let senderId = "sender-id"
        static let applicationId = "application-id"
        static let channelId = "channel-id"
        static let sessionId = "session-id"
        static let transactionId = "transaction-id"
        static let appVersion = "app-version"
        static let workflowId = "workflow-id"
        static let serviceTransactionId = "service-transaction-id"
        static let originApplicationId = "origin-application-id"
        static let subWorkflowId = "sub-workflow-id"
        static let activityId = "activity-id"
        static let storeId = "store-id"
        static let baggageId = "baggage-id"
        static let transactionType = "transaction-type"
        static let applicationUserId = "application-user-id"
        static let serviceGroup = "service-group"
        static let timeStamp = "timestamp"
        static let lineOfBusiness = "line-of-business"
        static let partnerId = "partnerId"
        static let authorization = "authorization"
        static let xauthorization = "x-authorization"
        static let xauthOriginator = "x-auth-originator"
        static let searchResultTemplate = "search-result-template"
        static let follow = "follow"
    }
}

// MARK: - API PROTOCOL ERRORS
public enum APIProtocolErrors: Error {
    case unencodableInput
    case unexpectedResponse
    case undecodableResponse
    case clientError(statusCode: Int)
    case serverError(statusCode: Int)
}

// MARK: - EXTENSION OF API PROTOCOL ERRORS
extension APIProtocolErrors: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unencodableInput:
            return "Unable to encode the provided input"
        case .unexpectedResponse:
            return "Unexpected Response"
        case .undecodableResponse:
            return "Unable to decode the response"
        case .clientError(statusCode: let statusCode):
            return "Transport error with status code \(statusCode)"
        case .serverError(statusCode: let statusCode):
            return "Server error with status code \(statusCode)"
        }
    }
}
