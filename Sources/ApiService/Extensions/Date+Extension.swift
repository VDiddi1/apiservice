//
//  Date+Extension.swift
//  ApiService
//
//

import Foundation

public extension Date {
    static var milliSecondsSince1970: String {
        let timeStampStr = String(describing: Int64(Date().timeIntervalSince1970 * 1000.0 .rounded()))
        return timeStampStr
    }
}
