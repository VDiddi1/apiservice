//
//  URLResponse+Extension.swift
//  ApiService
//
//  Created by Rohit Gupta on 2022-07-13.
//

import Foundation

extension HTTPURLResponse {
    /**
     Throw an APIProtocolErrors error if the status code falls within the 400 - 599 range. The function
     will either not throw an exception or raise a `clientError` or a `serverError`.
     */
    func throwForErrors() throws {
        if (400 ..< 500).contains(statusCode) {
            throw APIProtocolErrors.clientError(statusCode: statusCode)
        }

        if (500 ..< 600).contains(statusCode) {
            throw APIProtocolErrors.serverError(statusCode: statusCode)
        }
    }
}
