//
//  MockURLProtocol.swift
//  
//
//  Created by Michael Long on 8/26/22.
//

import Foundation

/// MockURLProtocol used to inject mock responses to URLs to assist in unit testing.
///
/// Example usage:
/// ```swift
/// func testSuccess() async throws {
///     // Initialize URLSession with MockURLProtocol
///     let configuration = URLSessionConfiguration.ephemeral
///     configuration.protocolClasses = [MockURLProtocol.self]
///     let mockUrlSession = URLSession(configuration: configuration)
///
///     // Initialize ApiService, passing in mockUrlSession
///     let apiService = ApiService(urlSession: mockUrlSession,  apiServiceDelegate: self, config:  [ .accessToken: "DummyAccessToken"])
///
///     // Setup mock response for www.test.com returning error json and http 404 status.
///     let url1 = URL(string: "http://www.test.com")
///     MockURLProtocol.response(for: url1, response: Error("{\"id\":7728}".utf8), statusCode: 404)
///     // Setup mock response for www.anothertest.com returning encodable object and statusCode: 200 (default)
///     let url2 = URL(string: "http://www.anothertest.com")
///     try MockURLProtocol.response(for: url2, response: MyEncodableObject(firstname: "Mike", lastName: "Long"))
///
///     // Perform API call and Assert response.
///     do {
///         let response: MyEncodableObject  = try await apiService.getRequest(to: url2 )
///         XCTAssertTrue(response.firstname == "Mike")
///     } catch {
///         XCTAssertTrue(false, error.localizedDescription)
///     }
/// }
/// ```
class MockURLProtocol: URLProtocol {
    
    private struct ResponseData {
        let data: Data
        let statusCode: Int
    }
    private static var data = [URL?: ResponseData]()
    
    /// Set a mock response
    /// - Parameters:
    ///   - url: URL to intercept
    ///   - response: Encodable response object to return
    ///   - statusCode: HTTP Status code to return. default = 200
    static func response<U: Encodable>(for url: URL?, response: U, statusCode: Int = 200) throws {
        MockURLProtocol.response(for: url, response: try JSONEncoder().encode(response), statusCode: statusCode)
    }
    
    /// Set a mock response
    /// - Parameters:
    ///   - url: URL to intercept
    ///   - response: Response Data object to return
    ///   - statusCode: HTTP Status code to return. default = 200
    static func response(for url: URL?, response: Data, statusCode: Int = 200) {
        MockURLProtocol.data[url] = ResponseData(data: response, statusCode: statusCode)
    }
    
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    /// Store the URL request and send success response back to the client.
    override func startLoading() {
        guard let client = client,
              let url = request.url else {
            fatalError("Client or URL missing")
        }
                
        guard let responseData = MockURLProtocol.data[url] else {
            fatalError("No Mock response for url: \(url.description)")
        }
        
        guard let response = HTTPURLResponse(url: url,
                                             statusCode: responseData.statusCode,
                                             httpVersion: nil,
                                             headerFields: nil) else { fatalError("Unable to build response") }
        
        client.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        client.urlProtocol(self, didLoad: responseData.data)
        
        client.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {
    }
}
